package step_definitions;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPageStepsDef {

    private final static By SHOP_BUTTON = By.xpath("//div/preceding-sibling::a[@href='/index.php/magazin']");
    private final static By BLOG_BUTTON = By.cssSelector(".uk-navbar-nav a[href='/index.php/news']");
    private final static By SUBSCRIBE_EMAIL_INPUT = By.cssSelector(".el-input");
    private final static By SUBSCRIBE_EMAIL_BUTTON = By.cssSelector(".el-button");
    private final static By SUBSCRIBE_RESULT_MESSAGE = By.cssSelector(".message");

    @И("^открыть сайт$")
    public void openPage() {
        open("https://qahacking.guru/");
    }

    @И("^перейти в магазин$")
    public void openCatalogPage() {
        $(SHOP_BUTTON).hover().click();
    }

    @И("^открыть \"Блог Джесси\"$")
    public void openBlogPage() {
        $(BLOG_BUTTON).click();
    }

    @И("^подписаться на рассылку$")
    public void subscribeToNews() {
        $(SUBSCRIBE_EMAIL_INPUT).scrollTo().setValue("test@test.com");
        $(SUBSCRIBE_EMAIL_BUTTON).click();
    }

    @И("^проверить результат подписки$")
    public void checkSubscription() {
        $(SUBSCRIBE_RESULT_MESSAGE).shouldNotHave(Condition.text("Bad Request"));
    }
}
