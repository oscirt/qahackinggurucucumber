package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class ProductInfoPageStepsDef {
    private final static By QUANTITY_INPUT = By.cssSelector("#quantity");
    private final static By WISHLIST_BUTTON = By.cssSelector(".btn-wishlist");

    @И("^указать количество товара \"(\\d*)\"$")
    public void setProductQuantity(int quantity) {
        $(QUANTITY_INPUT).setValue(Integer.toString(quantity));
    }

    @И("^добавить в список пожеланий$")
    public void addToWishList() {
        $(WISHLIST_BUTTON).click();
    }
}
