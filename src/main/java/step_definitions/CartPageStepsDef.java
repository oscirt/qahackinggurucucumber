package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartPageStepsDef {

    private final static By PRODUCT_COUNT = By.cssSelector(".data .inputbox");
    private final static By PRODUCT_NAME = By.cssSelector(".product_name .data");
    private final static By CHECKOUT_BUTTON = By.cssSelector(".btn-arrow-right");

    @И("^проверить количество при суммировании \"(\\d*)\" и \"(\\d*)\" товара \"(.*)\"$")
    public void checkProductCount(int first, int second, String productName) {
        SelenideElement card = $$(PRODUCT_NAME).findBy(Condition.text(productName)).parent().parent();
        card.find(PRODUCT_COUNT).shouldHave(Condition.value(Integer.toString(first + second)));
    }

    @И("^нажать на кнопку \"Оформить заказ\"$")
    public void checkout() {
        $(CHECKOUT_BUTTON).click();
    }
}
