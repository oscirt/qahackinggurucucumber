package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class AllBlogsPageStepsDef {

    private final static By POPULAR_CARD_DATE = By.cssSelector(".uk-margin-remove-first-child.uk-first-column div");
    private final static By CARD_LINK = By.cssSelector(".uk-link-reset");

    @И("^открыть популярную новость с датой \"(.*)\"$")
    public void openFamousNewsByDate(String date) {
        SelenideElement element = $$(POPULAR_CARD_DATE).findBy(Condition.text(date)).parent();
        element.find(CARD_LINK).click();
    }
}
