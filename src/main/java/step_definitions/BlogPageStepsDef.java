package step_definitions;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class BlogPageStepsDef {

    private final static By DATE = By.cssSelector("h3+.el-meta.uk-text-meta");
    private final static By NEXT_BUTTON = By.cssSelector(".uk-margin-auto-left a");
    private final static By BACK_BUTTON = By.cssSelector(".uk-margin-auto-right a");

    @И("^проверить дату новости с датой \"(.*)\"$")
    public void checkDate(String date) {
        $(DATE).shouldHave(Condition.text(date));
    }

    @И("^нажать на кнопку \"Вперед\"$")
    public void goNext() {
        $(NEXT_BUTTON).click();
    }

    @И("^нажать на кнопку \"Назад\"$")
    public void goBack() {
        $(BACK_BUTTON).click();
    }
}
