package step_definitions;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FormPageStepsDef {

    private final static By ACTIVE_STEP = By.cssSelector("#active_step");
    private final static By NAME_FIELD = By.cssSelector("#f_name");
    private final static By LAST_NAME_FIELD = By.cssSelector("#l_name");
    private final static By EMAIL_FIELD = By.cssSelector("#email");
    private final static By STREET_FIELD = By.cssSelector("#street");
    private final static By ZIP_FIELD = By.cssSelector("#zip");
    private final static By CITY_FIELD = By.cssSelector("#city");
    private final static By PHONE_FIELD = By.cssSelector("#phone");
    private final static By NEXT_BUTTON = By.cssSelector(".button");

    @И("^заполнить форму$")
    public void inputForm() {
        $(ACTIVE_STEP).shouldHave(Condition.text("Адрес"));
        $(NAME_FIELD).sendKeys("name");
        $(LAST_NAME_FIELD).sendKeys("last name");
        $(EMAIL_FIELD).sendKeys("email@mail.ru");
        $(STREET_FIELD).sendKeys("street");
        $(ZIP_FIELD).sendKeys("123456");
        $(CITY_FIELD).sendKeys("city");
        $(PHONE_FIELD).sendKeys("+213123123");
        $(NEXT_BUTTON).click();
    }
}
