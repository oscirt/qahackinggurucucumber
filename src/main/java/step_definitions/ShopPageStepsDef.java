package step_definitions;

import com.codeborne.selenide.Condition;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class ShopPageStepsDef {

    private final static By PRODUCT_NAME = By.cssSelector(".name");

    @И("^открыть подробности товара \"(.*)\"$")
    public void openProductInfo(String productName) {
        $$(PRODUCT_NAME).findBy(Condition.text(productName)).click();
    }
}
