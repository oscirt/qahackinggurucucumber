package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class WishListPageStepsDef {

    private final static By PRODUCT_NAME = By.cssSelector(".product_name .data");
    private final static By TO_CART_BUTTON = By.cssSelector("img[title='В корзину']");

    @И("^проверить название товара в списке пожеланий \"(.*)\"$")
    public void checkName(String productName) {
        $(PRODUCT_NAME).shouldHave(Condition.text(productName));
    }

    @И("^переместить товар \"(.*)\" в корзину$")
    public void addToCart(String productName) {
        SelenideElement card = $$(PRODUCT_NAME).findBy(Condition.text(productName)).parent().parent();
        card.find(TO_CART_BUTTON).click();
    }
}
