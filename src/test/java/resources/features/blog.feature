# language: ru
Функциональность: Блог
  @All
  Сценарий: Просмотр блога и дальнейшая подписка на рассылку фотографий
    * открыть сайт
    * открыть "Блог Джесси"
    * открыть популярную новость с датой "мая 15, 2022"
    * проверить дату новости с датой "мая 15, 2022"
    * нажать на кнопку "Вперед"
    * нажать на кнопку "Вперед"
    * нажать на кнопку "Назад"
    * подписаться на рассылку
    * проверить результат подписки